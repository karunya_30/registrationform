import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Form';
  registrationForm: FormGroup;
  constructor(private fb: FormBuilder) { }
  ngOnInit() {
    this.registrationForm = this.fb.group({
      userName: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.pattern('^[a-z0-9_-]{8,15}$')])],
      number:  ['', Validators.compose([Validators.required,Validators.maxLength(10), Validators.pattern('^[1-9]{1}[0-9]{9}')])],
      password: ['', ([Validators.required, Validators.minLength(6)])],
      email: ['', ([Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')])],
      gender: ['', Validators.required],
      conditions: ['', Validators.required],
      country: ['select', Validators.required],
      description :['']
    });
  }
  get f() {
    return this.registrationForm.controls;
  }
  get email() {
    return this.registrationForm.get('email');
  }
  onSubmit() {
    console.log(this.registrationForm.value);
  }

}
